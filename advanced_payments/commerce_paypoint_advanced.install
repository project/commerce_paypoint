<?php
/**
 * @file
 * Provides installation and update functions for the Advanced Payments service.
 */

/**
 * Implements hook_requirements().
 */
function commerce_paypoint_advanced_requirements($phase) {
  $requirements = array();

  if ($phase == 'runtime') {
    $instance_id = 'paypoint_advanced|commerce_payment_paypoint_advanced';
    $payment_method = commerce_payment_method_instance_load($instance_id);

    // If the settings form hasn't been saved yet we will have an empty settings
    // array. Guard against this case and return no status information.
    if (empty($payment_method['settings'])) {
      return array();
    }

    $settings = $payment_method['settings'];
    $merchant = $settings['merchant'];

    // Add a status message to display the mode of operation.
    switch ($settings['testing']['test_status']) {
      case COMMERCE_PAYPOINT_TEST_MODE_TEST:
        $description = t('All transactions will be returned as successful.');
        $severity = REQUIREMENT_WARNING;
        break;
      case COMMERCE_PAYPOINT_TEST_MODE_DECLINE:
        $description = t('All transactions will be returned as declined.');
        $severity = REQUIREMENT_WARNING;
        break;
      case COMMERCE_PAYPOINT_TEST_MODE_LIVE:
        $description = t('All transactions are being sent to the bank for authorisation.');
        $severity = REQUIREMENT_OK;
        break;
    }
    $requirements['paypoint_advanced_test_status'] = array(
      'title' => t('PayPoint Advanced Payments status'),
      'value' => _commerce_paypoint_test_mode_title_get($settings['testing']['test_status']),
      'description' => $description,
      'severity' => $severity,
    );

    // If we're in live mode, check that the merchant ID and password are
    // properly configured.
    if ($settings['testing']['test_status'] == COMMERCE_PAYPOINT_TEST_MODE_LIVE) {
      $errors = array();

      if (empty($merchant['username'])) {
        $errors[] = t('Username is missing.');
      }

      if (empty($merchant['password'])) {
        $errors[] = t('Password is missing.');
      }

      if (empty($merchant['instId'])) {
        $errors[] = t('Installation identifier is missing.');
      }

      if (!empty($errors)) {
        $requirements['paypoint_advanced_configuration'] = array(
          'title' => t('PayPoint Advanced Payments configuration'),
          'value' => t('Not properly configured'),
          'description' => implode(' ', $errors),
          'severity' => REQUIREMENT_ERROR,
        );
      }

    }
    $url = commerce_paypoint_advanced_server_url($settings['testing']['test_status']);

    // Check connectivity.
    $requirements['paypoint_advanced_endpoint'] = array(
      'title' => t('PayPoint Advanced Payments End Point URL'),
      'value' => $url,
      'severity' => REQUIREMENT_OK,
    );

    module_load_include('inc', 'commerce_paypoint_advanced', 'commerce_paypoint_advanced.class');
    $hosted = $settings['connectivity']['request_type'] == 'hosted';
    $ppo = new PPOAdvancedPayment($url, $merchant['username'], $merchant['password'], $hosted);
    try {
      $response = $ppo->sendPing();
    }
    catch (Exception $e) {
      watchdog_exception('commerce_paypoint_advanced', $e);
    }

    $requirements['paypoint_advanced_connectivity_test'] = array(
      'title' => t('PayPoint Connectivity Test'),
      'value' => $ppo->getLastStatus(),
      'severity' => @$response->status_message == 'OK' ? REQUIREMENT_OK : REQUIREMENT_ERROR,
    );

  }

  return $requirements;
}

