Commerce PayPoint Advanced Payments
=================================

Integrates PayPoint.net's Advanced Payments service with Drupal Commerce.

Integration guide: https://developer.paypoint.com/payments/docs/
